## Ansible репозиторий для провижна инфрастукткры
Для dev контура:
1) Установка Docker, добавление зеркал, добавление пользователя в группу docker
2) Запуск Portainer через docker-compose
3) Запуск Traefik с получением сертификата

Для prod контура:
1) Установка Certbot и получение сертификатов
2) Установка Nginx и замена default конфига
3) Установка Mysql с созданием пользователей и баз
4) Установка Docker, добавление зеркал, добавление пользователя в группу docker
5) Запуск Portainer через docker-compose
```
.
├── ansible.cfg
├── inventories
│   ├── dev
│   │   ├── files
│   │   │   └── env.mysql
│   │   ├── group_vars
│   │   │   └── all
│   │   │       └── main.yaml
│   │   ├── hosts
│   │   └── templates
│   │       ├── docker-proxy
│   │       │   └── docker-compose.yml.j2
│   │       ├── mirrors
│   │       │   └── daemon.json.j2
│   │       ├── portainer
│   │       │   └── docker-compose.yml.j2
│   │       └── traefik
│   │           └── docker-compose.yml.j2
│   └── prod
│       ├── group_vars
│       │   └── all
│       │       └── main.yaml
│       ├── hosts
│       └── templates
│           ├── mirrors
│           │   └── daemon.json.j2
│           ├── mysql
│           │   ├── my.cnf.j2
│           │   ├── root-my.cnf.j2
│           │   └── user-my.cnf.j2
│           ├── nginx
│           │   └── nginx.conf.j2
│           └── portainer
│               └── docker-compose.yml.j2
├── playbook.yaml
├── README.md
└── requirements.yaml
```

## CI/CD Variables
| Type     | Key             | Description                                                                                                  |
|----------|-----------------|--------------------------------------------------------------------------------------------------------------|
| FILE     | DEV_SSH_PRIVATE_KEY | Masked Key for accessing the VM.    
| FILE     | PROD_SSH_PRIVATE_KEY | Masked Key for accessing the VM.    
